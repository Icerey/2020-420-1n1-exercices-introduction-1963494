package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie6_String extends SimulConsole{
    @Test
    @Order(1)
    void test_string1() throws Exception {
        choixMenu("6a");
        ecrire("Bonjour");
        assertSortie("La longueur est: 7", false);
        choixMenu("6a");
        ecrire("allo");
        assertSortie("La longueur est: 4", false);

    }

    @Test
    @Order(2)
    void test_string2() throws Exception {
        choixMenu("6b");
        ecrire("1234");
        ecrire("1234");
        assertSortie("pareils", false);

        choixMenu("6b");
        ecrire("1234");
        ecrire("12345");
        assertSortie("différentes", false);
    }

    @Test
    @Order(3)
    void test_string3() throws Exception {
        choixMenu("6c");
        ecrire("bonjour");
        ecrire("BonJouR");
        assertSortie("pareils", false);

        choixMenu("6c");
        ecrire("allo");
        ecrire("allo");
        assertSortie("pareils", false);

        choixMenu("6c");
        ecrire("allo");
        ecrire("allo1");
        assertSortie("différentes", false);
    }

    @Test
    @Order(4)
    void test_string4() throws Exception {
        choixMenu("6d");
        ecrire("12345");
        assertSortie("La chaine est "+"\""+ 12345 +"\""+".", false);

    }
    @Test
    @Order(5)
    void test_string5() throws Exception {
        choixMenu("6e");
        ecrire("12345");
        ecrire("5678");
        assertSortie("\"12345\"\\\"5678\"", false);

    }
    @Test
    @Order(6)
    void test_string6() throws Exception {
        choixMenu("6f");
        ecrire("bonjour");
        ecrire("3");
        assertSortie("j", false);

        choixMenu("6f");
        ecrire("allo");
        ecrire("7");
        assertSortie("invalide", false);

        choixMenu("6f");
        ecrire("bonjour");
        ecrire("-2");
        assertSortie("invalide", false);

    }

}
